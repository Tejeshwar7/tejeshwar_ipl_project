/**
 * #### This module accepts the data from matches and returns the number of matches played each year
 * @module matchesPlayedPerYear
 * 
 */
/**
 *
 * @param {Object} Matches Parsed JSON data of matches from matches.csv file
 * @returns {object} Returns Number of matches played each year 
 */


function matchesPLayedPerYear(matches) {

    let result = matches.reduce((matchesPerYear, match) => {
        let year = match.season
        if (matchesPerYear[year]) {
            matchesPerYear[year] += 1
        } else {
            matchesPerYear[year] = 1
        }
        return matchesPerYear

    }, {})
    return result

}

module.exports = matchesPLayedPerYear