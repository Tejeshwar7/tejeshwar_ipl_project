/**
 *#### This module accepts the data from matches,deliveries and year as a parameter and returns Top Economic Bowlers of that particular year 
 * @module EconomicBowlers
 *
 */
/**
 * 
 * @param {Object} Matches Parsed JSON data of matches from matches.csv file
 * @param {Object} Deliveries Parsed JSON data of delivers from deliveries.csv file
 * @param {string} Year Parameter
 * @returns {Object} Returns Top Economic Bowlers of that particular year 
 */





function economicalBowlers(matches, deliveries) {

    const ids = matches.filter(obj => obj.season === '2015').map(obj => parseInt(obj.id));

    const deliveries2015 = deliveries.filter(val => ids.includes(parseInt(val.match_id)));

    const totalRunsBalls = deliveries2015.reduce((total, delivery) => {
        const bowler = delivery.bowler;

        if (total[bowler]) {
            total[bowler].bowls += 1;
            total[bowler].concededRun += parseInt(delivery.total_runs);
            total[bowler].economyRate = +((total[bowler].concededRun / ((total[bowler].bowls) / 6)).toFixed(3));

        } else {

            total[bowler] = {};
            total[bowler].bowls = 1;
            total[bowler].concededRun = parseInt(delivery.total_runs);
            total[bowler].economyRate = +((total[bowler].concededRun / ((total[bowler].bowls) / 6)).toFixed(3));
        }

        return total;
    }, {});

    const topTenEconomicalBowler = Object.entries(totalRunsBalls).sort((a, b) => a[1].economyRate - b[1].economyRate).slice(0, 10);
    let result = Object.fromEntries(topTenEconomicalBowler)

    return result

}





module.exports = economicalBowlers;