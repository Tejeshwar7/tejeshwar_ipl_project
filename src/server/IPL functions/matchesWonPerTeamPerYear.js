/**
 * #### This module accepts the data from matches and return the number of matches won each team per year
 * @module matchesWonPerTeamPerYear
 *
 */

/**
 * 
 * @param {Object} Matches Parsed JSON data of matches from matches.csv file
 * @returns {Object} Returns number of matches won each team per year
 */


function matchesWonPerTeamPerYear(matches){
  let result = matches.reduce((matchesWonByTeam,match)=>{
      let year = match.season
      let winner = match.winner
  
      if(matchesWonByTeam[year]){
          if(matchesWonByTeam[year][winner]){
              matchesWonByTeam[year][winner] += 1
          }else{
              matchesWonByTeam[year][winner] = 1
          }
  
      }else{
          matchesWonByTeam[year] = {}
      }
      return matchesWonByTeam
  
  },{})
  
  return result
  
  
  }
  
  module.exports= matchesWonPerTeamPerYear
  