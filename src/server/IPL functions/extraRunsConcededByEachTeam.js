/**
 * #### This Module accepts data from matches,deliveries,year and returns Extra Runs Conceded Per Team
 * @module extraRunsConcededPerTeam
 */


/**
 * 
 * @param {Object} Matches Parsed JSON data of matches from matches.csv file
 * @param {Object} Deliveries Parsed JSON data of delivers from deliveries.csv file
 * @param {String} Year Parameter
 * @returns {Object} Returns Extra Runs Conceded Per Team
 */






function extraRunsConcededByEachTeam(matches, deliveries) {

    let ids = matches.filter(year => year.season === "2016").map(obj => parseInt(obj.id))
    let deliveryData = deliveries.filter(data => ids.includes(parseInt(data.match_id)))

    let result = deliveryData.reduce((extraRun, delivery) => {
        let bowling_team = delivery["bowling_team"]
        if (extraRun[bowling_team]) {
            extraRun[bowling_team] += parseInt(delivery["extra_runs"])
        } else {
            extraRun[bowling_team] = parseInt(delivery["extra_runs"])
        }
        return extraRun

    }, {})


    return result
}

module.exports = extraRunsConcededByEachTeam