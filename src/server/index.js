const fs = require("fs"); //internal function
const csv = require("csvtojson"); // external function


const matchesPlayedPerYear = require("./IPL functions/matchesPlayedPerYear.js") //written function
const matchesWonPerTeamPerYear = require("/home/teje/IPL/src/server/IPL functions/matchesWonPerTeamPerYear.js") //written function
const extraRunsConcededByEachTeam = require("/home/teje/IPL/src/server/IPL functions/extraRunsConcededByEachTeam.js") //written function
const economicBowlers = require("/home/teje/IPL/src/server/IPL functions/economicBowlers.js") //written function

//reading the file

/**
 * This contains path to matches.csv
 * @type {string}  
 */

const MATCHES_FILE_PATH = "src/data/matches.csv";

//reading the file

/**
 * This contains path to deliveries.csv
 * @type {string}  
 */


const MATCHES_FILE_PATH1 = "src/data/deliveries.csv"

//output files(PATH)


const JSON_OUTPUT_FILE_PATH = "src/public/output/matchesPerYear.json"
const JSON_OUTPUT_FILE_PATH1 = "src/public/output/matchesWonPerTeamPerYear.json";
const JSON_OUTPUT_FILE_PATH2 = "src/public/output/extraRunsConcededByEachTeam.json"
const JSON_OUTPUT_FILE_PATH3 = "src/public/output/economicBowlers.json";



function main() {
  csv()
    .fromFile(MATCHES_FILE_PATH)
    .then(matches => {
      csv()
        .fromFile(MATCHES_FILE_PATH1)
        .then(deliveries => {
          let result = matchesPlayedPerYear(matches);
          saveMatchesPerYear(result);
          let result1 = matchesWonPerTeamPerYear(matches);
          saveMatchesWonPerTeamPerYear(result1);
          let result2 = extraRunsConcededByEachTeam(matches, deliveries, "2016");
          saveExtraRunsConcededByEachTeam(result2);
          let result3 = economicBowlers(matches, deliveries, "2015");
          saveEconomicBowlers(result3)


        })

    });
}

/**
 * @param {matchesPlayedPerYear}
 * @param {Object}
 */

function saveMatchesPerYear(result) {
  const matchesPerYear = {
    matchesPlayedPerYear: result
  };
  const jsonString = JSON.stringify(matchesPerYear);
  fs.writeFile(JSON_OUTPUT_FILE_PATH, jsonString, err => {
    if (err) {
      console.error(err);
    }
  });
}

/**
 * @param {matchesWonPerTeamPerYearResult}
 * @param {Object}
 */



function saveMatchesWonPerTeamPerYear(result1) {
  const matchesWonPerTeam = {
    matchesWonPerTeamPerYear: result1
  };
  const jsonString1 = JSON.stringify(matchesWonPerTeam);
  fs.writeFile(JSON_OUTPUT_FILE_PATH1, jsonString1, err => {
    if (err) {
      console.error(err);
    }
  })
}

/**
 * @param {extraRunsConcededByEachTeam}
 * @param {Object}
 */



function saveExtraRunsConcededByEachTeam(result2) {
  const extraRunsConceded = {
    extraRunsConcededByEachTeam: result2
  };
  const jsonString2 = JSON.stringify(extraRunsConceded);
  fs.writeFile(JSON_OUTPUT_FILE_PATH2, jsonString2, err => {
    if (err) {
      console.error(err);
    }
  })
}

/**
 * @param {economicBowlers}
 * @param {Object}
 */



function saveEconomicBowlers(result3) {
  const ecoBowlers = {
    economicBowlers: result3
  };
  const jsonString3 = JSON.stringify(ecoBowlers);
  fs.writeFile(JSON_OUTPUT_FILE_PATH3, jsonString3, err => {
    if (err) {
      console.error(err);
    }
  })
}


main();